package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
