package ru.ekfedorov.tm.api.entity;

import java.util.Date;

public interface IHasDateFinish {

    Date getDateFinish();

    void setDateFinish(Date dateFinish);

}
