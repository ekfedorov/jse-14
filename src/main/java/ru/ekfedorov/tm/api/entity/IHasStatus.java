package ru.ekfedorov.tm.api.entity;

import ru.ekfedorov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
