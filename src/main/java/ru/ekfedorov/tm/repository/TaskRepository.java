package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        final List<Task> task = new ArrayList<>(list);
        task.sort(comparator);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        if (isEmpty(projectId)) return null;
        final List<Task> list1 = new ArrayList<>();
        for (Task task: list) {
            if (projectId.equals(task.getProjectId())) list1.add(task);
        }
        return list1;
    }

    @Override
    public List<Task> removeAllByProjectId(String projectId) {
        if (isEmpty(projectId)) return null;
        list.removeIf(task -> projectId.equals(task.getProjectId()));
        return list;
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProjectId(String taskId) {
        Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task: list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task: list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

}
