package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository, final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (isEmpty(projectId)) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProject(final String projectId, final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) return null;
        return taskRepository.bindTaskByProjectId(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (isEmpty(taskId)) return null;
        return taskRepository.unbindTaskFromProjectId(taskId);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (isEmpty(projectId)) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

}
