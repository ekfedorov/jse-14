package ru.ekfedorov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_COMMANDS = "commands";

    String CMD_ARGUMENTS = "arguments";

    String CMD_TASK_CREATE = "task-create";

    String CMD_TASK_CLEAR = "task-clear";

    String CMD_TASK_LIST = "task-list";

    String CMD_PROJECT_CREATE = "project-create";

    String CMD_PROJECT_CLEAR = "project-clear";

    String CMD_PROJECT_LIST = "project-list";

    String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";

    String CMD_TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String CMD_TASK_VIEW_BY_INDEX = "task-view-by-index";

    String CMD_TASK_VIEW_BY_NAME = "task-view-by-name";

    String CMD_TASK_VIEW_BY_ID = "task-view-by-id";

    String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String CMD_PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String CMD_PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";

    String CMD_START_PROJECT_BY_ID = "start-project-by-id";

    String CMD_START_PROJECT_BY_INDEX = "start-project-by-index";

    String CMD_START_PROJECT_BY_NAME = "start-project-by-name";

    String CMD_FINISH_PROJECT_BY_ID = "finish-project-by-id";

    String CMD_FINISH_PROJECT_BY_INDEX = "finish-project-by-index";

    String CMD_FINISH_PROJECT_BY_NAME = "finish-project-by-name";

    String CMD_CHANGE_PROJECT_STATUS_BY_ID = "change-project-status-by-id";

    String CMD_CHANGE_PROJECT_STATUS_BY_INDEX = "change-project-status-by-index";

    String CMD_CHANGE_PROJECT_STATUS_BY_NAME = "change-project-status-by-name";

    String CMD_START_TASK_BY_ID = "start-task-by-id";

    String CMD_START_TASK_BY_INDEX = "start-task-by-index";

    String CMD_START_TASK_BY_NAME = "start-task-by-name";

    String CMD_FINISH_TASK_BY_ID = "finish-task-by-id";

    String CMD_FINISH_TASK_BY_INDEX = "finish-task-by-index";

    String CMD_FINISH_TASK_BY_NAME = "finish-task-by-name";

    String CMD_CHANGE_TASK_STATUS_BY_ID = "change-task-status-by-id";

    String CMD_CHANGE_TASK_STATUS_BY_INDEX = "change-task-status-by-index";

    String CMD_CHANGE_TASK_STATUS_BY_NAME = "change-task-status-by-name";

    String CMD_REMOVE_PROJECT_WITH_TASKS_BY_ID = "remove-project-with-tasks-by-id";

    String CMD_FIND_AL_LBY_PROJECT_ID = "find-all-by-project-id";

    String CMD_BIND_TASK_BY_PROJECT = "bind-task-by-project";

    String CMD_UNBIND_TASK_FROM_PROJECT = "unbind-task-from-project";

}
